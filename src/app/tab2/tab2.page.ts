'use strict';

import { Component } from '@angular/core';
import axios from 'axios';



@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  countries:[]

  constructor() {
    this.loadCountries('',false);
  }

  loadCountries(sort:string,showYesterday:boolean){
    axios.get('https://disease.sh/v3/covid-19/countries?yesterday='+showYesterday+'&sort='+sort)
    .then((response)=>{
      this.countries = response.data;
      console.log(this.countries);
    }).catch((error)=>{
      console.log(error);
    })
  }

}
