'use strict';

import { Component } from '@angular/core';
import axios from 'axios';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  continent:[];

  constructor() {
    this.loadCountries();
  }

  loadCountries(strict:boolean=true,showYesterday:boolean=false,continent=''){
    axios.get('https://disease.sh/v3/covid-19/continents/'+continent+'?yesterday='+showYesterday+'&strict='+strict)
    .then((response)=>{
      this.continent = response.data;
      console.log(response);
    }).catch((error)=>{
      console.log(error);
    })
  }

}
