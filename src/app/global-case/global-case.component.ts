import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-global-case',
  templateUrl: './global-case.component.html',
  styleUrls: ['./global-case.component.scss'],
})
export class GlobalCaseComponent implements OnInit {
  @Input() name: string;
  constructor() { }

  ngOnInit() {}

}
