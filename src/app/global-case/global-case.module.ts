import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GlobalCaseComponent } from './global-case.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule],
  declarations: [GlobalCaseComponent],
  exports: [GlobalCaseComponent]
})
export class GlobalCaseComponentModule {}
