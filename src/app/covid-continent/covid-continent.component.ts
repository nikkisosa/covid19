import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-covid-continent',
  templateUrl: './covid-continent.component.html',
  styleUrls: ['./covid-continent.component.scss'],
})
export class CovidContinentComponent implements OnInit {
  @Input() continent: [];

  country:[];

  constructor() {}

  ngOnInit() {}

  toArray(answers: object) {
    return Object.keys(answers).map(key => answers[key])
  }

  timestampTodate(unixTime:number){
    var date = new Date(unixTime).toISOString()

    var _date = new Date(date);


    var days          =  ["Mon", 
                      "Tues", 
                      "Weds", 
                      "Thurs", 
                      "Fri", 
                      "Sat", 
                      "Sun"];
    var months        =  ["January",
                      "February",
                      "March",
                      "April",
                      "May",
                      "June",
                      "July",
                      "August",
                      "September",
                      "October",
                      "November",
                      "December"];
    var dayDigit      =  _date.getDate();
    var day           =  days[_date.getDay()];
    var month         =  months[_date.getMonth()];
    var hours         =  _date.getHours();
    var minutes       =  _date.getMinutes();
    var seconds       =  ("0" + _date.getSeconds()).slice(-2);

    return _date.toUTCString();
  }


}
