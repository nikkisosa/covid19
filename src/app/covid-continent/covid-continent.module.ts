import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CovidContinentComponent } from './covid-continent.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule],
  declarations: [CovidContinentComponent],
  exports: [CovidContinentComponent]
})
export class CovidContinentComponentModule {}
