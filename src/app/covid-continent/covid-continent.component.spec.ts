import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CovidContinentComponent } from './covid-continent.component';

describe('CovidContinentComponent', () => {
  let component: CovidContinentComponent;
  let fixture: ComponentFixture<CovidContinentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidContinentComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CovidContinentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
