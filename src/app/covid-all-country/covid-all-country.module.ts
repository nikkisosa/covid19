import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CovidAllCountryComponent } from './covid-all-country.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule],
  declarations: [CovidAllCountryComponent],
  exports: [CovidAllCountryComponent]
})
export class CovidAllCountryComponentModule {}
