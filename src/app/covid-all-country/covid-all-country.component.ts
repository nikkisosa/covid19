import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-covid-all-country',
  templateUrl: './covid-all-country.component.html',
  styleUrls: ['./covid-all-country.component.scss'],
})
export class CovidAllCountryComponent implements OnInit {
  @Input() country: [];
  constructor() { }

  ngOnInit() {}

}
